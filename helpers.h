#ifndef _AUX_H_
#define _AUX_H_

#include <sys/types.h>

#define HZ 1
/* amount of data to read in one read() call */
#define AUDIO_DEFAULT_FILECHUNK      (1024*32)

#ifndef MIN
#define MIN(a, b) (((a)<(b))?(a):(b))
#endif

#define DEBUGF printf

off_t filesize(int fd);

#endif
