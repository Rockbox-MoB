/***************************************************************************
 *             __________               __   ___.
 *   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
 *   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
 *   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
 *   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
 *                     \/            \/     \/    \/            \/
 * $Id$
 *
 * Copyright (C) 2007 Nicolas Pennequin
 *
 * All files in this archive are subject to the GNU General Public License.
 * See the file COPYING in the source tree root for full license agreement.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ****************************************************************************/

#ifndef _BUFFERING_H_
#define _BUFFERING_H_

#include <sys/types.h>

#define MAX_PATH 256

enum data_type {
    TYPE_CODEC,
    TYPE_AUDIO,
    TYPE_STREAM,
    TYPE_ID3,
    TYPE_CUESHEET,
    TYPE_IMAGE,
    TYPE_BUFFER,
    TYPE_UNKNOWN,
};

void buffer_init(void);
int bufopen(char *file, size_t offset, enum data_type type);
int bufalloc(void *src, size_t size, enum data_type type);
int bufclose(int handle_id);
int bufseek(int handle_id, size_t newpos);
int bufadvance(int handle_id, off_t offset);
ssize_t bufread(int handle_id, size_t size, char *dest);
ssize_t bufgetdata(int handle_id, size_t size, unsigned char **data);

#endif
