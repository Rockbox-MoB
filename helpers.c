#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "helpers.h"

off_t filesize(int fd)
{
    struct stat status;
    fstat(fd, &status);
    return status.st_size;
}
