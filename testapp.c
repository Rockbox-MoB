/***************************************************************************
 *             __________               __   ___.
 *   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
 *   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
 *   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
 *   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
 *                     \/            \/     \/    \/            \/
 * $Id$
 *
 * Copyright (C) 2007 Nicolas Pennequin
 *
 * All files in this archive are subject to the GNU General Public License.
 * See the file COPYING in the source tree root for full license agreement.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "buffering.h"
#include "helpers.h"

#define BUFFER_SIZE  (32*1024*1024)
#define GUARD_SIZE   (32*1024)

/* Ring buffer helper macros */
/* Buffer pointer (p) plus value (v), wrapped if necessary */
#define RINGBUF_ADD(p,v) ((p+v)<buffer_len ? p+v : p+v-buffer_len)
/* Buffer pointer (p) minus value (v), wrapped if necessary */
#define RINGBUF_SUB(p,v) ((p>=v) ? p-v : p+buffer_len-v)
/* How far value (v) plus buffer pointer (p1) will cross buffer pointer (p2) */
#define RINGBUF_ADD_CROSS(p1,v,p2) \
((p1<p2) ? (int)(p1+v)-(int)p2 : (int)(p1+v-p2)-(int)buffer_len)
/* Bytes available in the buffer */
#define BUF_USED RINGBUF_SUB(buf_widx, buf_ridx)

struct memory_handle {
    int id;              /* A unique ID for the handle */
    enum data_type type;
    enum data_state state;
    char path[MAX_PATH];
    int fd;
    size_t data;         /* Start index of the handle's data buffer */
    size_t data_len;     /* Length of the data buffer for the handle */
    size_t ridx;         /* Current read pointer, relative to the main buffer */
    size_t widx;         /* Current write pointer */
    size_t filesize;     /* File total length */
    size_t filerem;      /* Remaining bytes of file NOT in buffer */
    size_t available;    /* Available bytes to read from buffer */
    size_t offset;       /* Offset at which we started reading the file */
    struct memory_handle *next;
};

static char *buffer;
static char *guard_buffer;

static size_t buffer_len;
static size_t buf_widx;
static size_t buf_ridx;

static size_t conf_filechunk;

/* current memory handle in the linked list. NULL when the list is empty. */
static struct memory_handle *cur_handle;
/* first memory handle in the linked list. NULL when the list is empty. */
static struct memory_handle *first_handle;
static int num_handles;

static void graph_view(int width);


/* add a new handle to the linked list and return it. It will have become the
   new current handle. The handle will reserve "data_size" bytes or if that's
   not possible, decrease "data_size" to allow adding the handle. */
static struct memory_handle *add_handle(int *data_size)
{
    /* this will give each handle a unique id */
    static int cur_handle_id = 0;

    int len = (data_size ? *data_size : 0) + sizeof(struct memory_handle);

    /* check that we actually can add the handle and its data */
    int overlap = RINGBUF_ADD_CROSS(buf_widx, len + 3, buf_ridx);
    if (overlap >= 0) {
        *data_size -= overlap;
        len -= overlap;
    }
    if (len < sizeof(struct memory_handle)) {
        return NULL;
    }

    /* make sure buf_widx is 32-bit aligned so that the handle struct is. */
    buf_widx = (RINGBUF_ADD(buf_widx, 3)) & ~3;

    struct memory_handle *new_handle = (struct memory_handle *)(buffer + buf_widx);

    /* only advance the buffer write index of the size of the struct */
    buf_widx = RINGBUF_ADD(buf_widx, sizeof(struct memory_handle));

    if (!first_handle) {
        /* the new handle is the first one */
        first_handle = new_handle;
    }

    if (cur_handle) {
        cur_handle->next = new_handle;
    }

    cur_handle = new_handle;
    cur_handle->id = cur_handle_id++;
    cur_handle->next = NULL;
    num_handles++;
    return cur_handle;
}

/* delete a given memory handle from the linked list
   and return true for success. Nothing is actually erased from memory. */
static bool rm_handle(struct memory_handle *h)
{
    if (h == first_handle) {
        first_handle = h->next;
        if (h == cur_handle) {
            DEBUGF("removing the first and last handle\n");
            cur_handle = NULL;
            buf_ridx = buf_widx;
        } else {
            buf_ridx = (void *)first_handle - (void *)buffer;
        }
    } else {
        struct memory_handle *m = first_handle;
        while (m && m->next != h) {
            m = m->next;
        }
        if (h && m && m->next == h) {
            m->next = h->next;
            if (h == cur_handle) {
                cur_handle = m;
            }
        } else {
            return false;
        }
    }

    num_handles--;
    return true;
}

/* these are unfortunalty needed to be global
    so move_handle can invalidate them */
static int cached_handle_id = -1;
static struct memory_handle *cached_handle = NULL;

/* Return a pointer to the memory handle of given ID.
   NULL if the handle wasn't found */
static struct memory_handle *find_handle(int handle_id)
{
    /* simple caching because most of the time the requested handle
    will either be the same as the last, or the one after the last */
    if (cached_handle)
    {
        if (cached_handle_id == handle_id && cached_handle_id == cached_handle->id)
            return cached_handle;
        else if (cached_handle->next && (cached_handle->next->id == handle_id))
        {
            /* JD's quick testing showd this block was only entered
               2/1971 calls to find_handle.
               8/1971 calls to find_handle resulted in a cache miss */
            cached_handle = cached_handle->next;
            cached_handle_id = handle_id;
            return cached_handle;
        }
    }

    struct memory_handle *m = first_handle;
    while (m && m->id != handle_id) {
        m = m->next;
    }
    cached_handle_id = handle_id;
    cached_handle = m;
    return (m && m->id == handle_id) ? m : NULL;
}

/* Move a memory handle to newpos.
   Return a pointer to the new location of the handle */
static struct memory_handle *move_handle(size_t newpos, struct memory_handle *h)
{
    /* make sure newpos is 32-bit aligned so that the handle struct is. */
    newpos = (RINGBUF_ADD(newpos, 3)) & ~3;

    DEBUGF("move_handle\n");
    struct memory_handle *dest = (struct memory_handle *)(buffer + newpos);

    /* Invalidate the cache to prevent it from keeping the old location of h */
    if (h == cached_handle)
        cached_handle = NULL;

    /* the cur_handle pointer might need updating */
    if (h == cur_handle) {
        cur_handle = dest;
    }

    if (h == first_handle) {
        first_handle = dest;
        buf_ridx = newpos;
    } else {
        struct memory_handle *m = first_handle;
        while (m && m->next != h) {
            m = m->next;
        }
        if (h && m && m->next == h) {
            m->next = dest;
        } else {
            return NULL;
        }
    }
    memmove(dest, h, sizeof(struct memory_handle));
    return dest;
}
/* Buffer data for the given handle. Return the amount of data buffered
   or -1 if the handle wasn't found */
static int buffer_handle(int handle_id)
{
    struct memory_handle *h = find_handle(handle_id);
    if (!h)
        return -1;

    if (h->filerem == 0) {
        /* nothing left to buffer */
        return 0;
    }

    if (h->fd < 0)  /* file closed, reopen */
    {
        if (*h->path)
            h->fd = open(h->path, O_RDONLY);
        else
            return -1;

        if (h->fd < 0)
            return -1;

        if (h->offset)
            lseek(h->fd, h->offset, SEEK_SET);
    }

    int ret = 0;
    while (h->filerem > 0)
    {
        //DEBUGF("h: %d\n", (void *)h - (void *)buffer);
        //DEBUGF("buf_widx: %ld\n", (long)buf_widx);
        /* max amount to copy */
        size_t copy_n = MIN(conf_filechunk, buffer_len - buf_widx);

        /* stop copying if it would overwrite the reading position */
        if (RINGBUF_ADD_CROSS(buf_widx, copy_n, buf_ridx) >= 0)
            break;

        /* rc is the actual amount read */
        int rc = read(h->fd, &buffer[buf_widx], copy_n);

        if (rc < 0)
        {
            //DEBUGF("failed on fd %d at buf_widx = %ld for handle %d\n", h->fd, (long)buf_widx, h->id);
            DEBUGF("File ended %ld bytes early\n", (long)h->filerem);
            h->filesize -= h->filerem;
            h->filerem = 0;
            break;
        }

        /* Advance buffer */
        h->widx = RINGBUF_ADD(h->widx, rc);
        if (h == cur_handle)
            buf_widx = h->widx;
        h->available += rc;
        ret += rc;
        h->filerem -= rc;
    }

    if (h->filerem == 0) {
        /* finished buffering the file */
        close(h->fd);
    }

    DEBUGF("buffered %d bytes (%d of %d available, remaining: %d)\n",
           ret, h->available, h->filesize, h->filerem);
    return ret;
}

/* Free buffer space by moving the first handle struct right before the useful
   part of its data buffer */
static void free_buffer(int handle_id)
{
    DEBUGF("free_buffer(%d)\n", handle_id);
    struct memory_handle *h = find_handle(handle_id);
    if (!h)
        return;

    size_t delta = RINGBUF_SUB(h->ridx, h->data);
    size_t dest = RINGBUF_ADD((void *)h - (void *)buffer, delta);
    //DEBUGF("buf_ridx: %ld, buf_widx: %ld\n", (long)buf_ridx, (long)buf_widx);
    //DEBUGF("dest: %ld, delta: %ld\n", (long)dest, (long)delta);
    h = move_handle(dest, h);
    h->data = RINGBUF_ADD(h->data, delta);
    h->ridx = h->data;
    h->available -= delta;
    graph_view(100);
}

/* Request a file be buffered
    filename: name of the file t open
    offset:   starting offset to buffer from the file
    RETURNS:  <0 if the file cannot be opened, or one file already
                queued to be opened, otherwise the handle for the file in the buffer
*/
int bufopen(char *file, size_t offset)
{
    /* add the file to the buffering queue. */
    /* for now, we'll assume the queue is always empty, so the handle
       gets added immediately */

    DEBUGF("bufopen: %s (offset: %d)\n", file, offset);

    int fd = open(file, O_RDONLY);
    if (fd < 0)
        return -1;

    if (offset)
        lseek(fd, offset, SEEK_SET);

    int size = filesize(fd) - offset;
    struct memory_handle *h = add_handle(&size);
    if (!h)
    {
        DEBUGF("failed to add handle\n");
        return -1;
    }

    strncpy(h->path, file, MAX_PATH);
    h->fd = fd;
    h->filesize = filesize(fd);
    h->filerem = h->filesize - offset;
    h->offset = offset;
    h->ridx = buf_widx;
    h->widx = buf_widx;
    h->data = buf_widx;
    h->data_len = size;
    h->available = 0;

    DEBUGF("added handle : %d\n", h->id);
    return h->id;
}

/* Close the handle. Return 0 for success and < 0 for failure */
int bufclose(int handle_id)
{
    DEBUGF("bufclose(%d)\n", handle_id);
    struct memory_handle *h = find_handle(handle_id);
    if (!h)
        return -1;

    rm_handle(h);
    return 0;
}

/* Set the reading index in a handle (relatively to the start of the handle data).
   Return 0 for success and < 0 for failure */
int bufseek(int handle_id, size_t offset)
{
    struct memory_handle *h = find_handle(handle_id);
    if (!h)
        return -1;

    if (offset > h->available)
        return -2;

    h->ridx = RINGBUF_ADD(h->data, offset);
    return 0;
}

/* Advance the reading index in a handle (relatively to its current position).
   Return 0 for success and < 0 for failure */
int bufadvance(int handle_id, ssize_t offset)
{
    struct memory_handle *h = find_handle(handle_id);
    if (!h)
        return -1;

    if (offset >= 0)
    {
        if (offset > h->available - RINGBUF_SUB(h->ridx, h->data))
            return -2;

        h->ridx = RINGBUF_ADD(h->ridx, offset);
    }
    else
    {
        if (-offset > RINGBUF_SUB(h->ridx, h->data))
            return -2;

        h->ridx = RINGBUF_SUB(h->ridx, -offset);
    }

    return 0;
}

/* Copy data from the given handle to the dest buffer.
   Return the number of bytes copied or -1 for failure. */
int bufread(int handle_id, size_t size, char *dest)
{
    struct memory_handle *h = find_handle(handle_id);
    size_t buffered_data;
    if (!h)
        return -1;
    if (h->available == 0)
        return 0;
    buffered_data = MIN(size, h->available);

    if (h->ridx + buffered_data > buffer_len)
    {
        size_t read = buffer_len - h->ridx;
        memcpy(dest, &buffer[h->ridx], read);
        memcpy(dest+read, buffer, buffered_data - read);
    }
    else memcpy(dest, &buffer[h->ridx], buffered_data);

    h->ridx = RINGBUF_ADD(h->ridx, buffered_data);
    h->available -= buffered_data;
    return buffered_data;
}

/* Update the "data" pointer to make the handle's data available to the caller.
   Return the length of the available linear data or -1 for failure. */
long bufgetdata(int handle_id, size_t size, unsigned char **data)
{
    struct memory_handle *h = find_handle(handle_id);
    if (!h)
        return -1;

    long ret;

    if (h->ridx + size > buffer_len &&
        h->available - RINGBUF_SUB(h->ridx, h->data) >= size)
    {
        /* use the guard buffer to provide what was requested. */
        int copy_n = h->ridx + size - buffer_len;
        memcpy(guard_buffer, (unsigned char *)buffer, copy_n);
        ret = size;
    }
    else
    {
        ret = MIN(h->available - RINGBUF_SUB(h->ridx, h->data),
                  buffer_len - h->ridx);
    }

    *data = (unsigned char *)(buffer + h->ridx);

    //DEBUGF("bufgetdata(%d): h->ridx=%ld, ret=%ld\n", handle_id, (long)h->ridx, ret);
    return ret;
}

bool test_ll(void)
{
    struct memory_handle *m1, *m2, *m3, *m4;

    if (cur_handle != NULL || first_handle != NULL)
        return false;

    m1 = add_handle(NULL);

    if (cur_handle != m1 || first_handle != m1 || m1->next != NULL)
        return false;

    m2 = add_handle(NULL);

    if (cur_handle != m2 || first_handle != m1 || m1->next != m2 || m2->next != NULL)
        return false;

    m3 = add_handle(NULL);

    if (cur_handle != m3 || first_handle != m1 || m2->next != m3 || m3->next != NULL)
        return false;

    rm_handle(m2);

    if (cur_handle != m3 || first_handle != m1 || m1->next != m3)
        return false;

    rm_handle(m3);

    if (cur_handle != m1 || first_handle != m1 || m1->next != NULL)
        return false;

    m4 = add_handle(NULL);

    if (cur_handle != m4 || first_handle != m1 || m1->next != m4 || m4->next != NULL)
        return false;

    rm_handle(m1);

    if (cur_handle != m4 || first_handle != m4)
        return false;

    rm_handle(m4);

    if (cur_handle != NULL || first_handle != NULL)
        return false;

    m1 = add_handle(NULL);
    m2 = add_handle(NULL);
    m3 = add_handle(NULL);
    m4 = add_handle(NULL);

    if (cur_handle != m4 || first_handle != m1)
        return false;

    m2 = move_handle(RINGBUF_ADD(m2->data, 1024*1024), m2);

    if (cur_handle != m4 || first_handle != m1 || m1->next != m2 || m2->next != m3)
        return false;

    m1 = move_handle(RINGBUF_ADD(m1->data, 1024*1024*3), m1);

    if (cur_handle != m4 || first_handle != m1 || m1->next != m2)
        return false;

    rm_handle(m1);
    rm_handle(m2);
    rm_handle(m3);
    rm_handle(m4);

    if (cur_handle != NULL || first_handle != NULL)
        return false;

    return true;
}

#if 0
static void list_handles(void)
{
    struct memory_handle *m = first_handle;
    while (m) {
        DEBUGF("%02d - %d\n", m->id, (void *)m-(void *)buffer);
        m = m->next;
    }
}
#endif

/* display a nice graphical view of the ringbuffer. */
static void graph_view(int width)
{
    int i, r_pos, w_pos;
    r_pos = buf_ridx * width / buffer_len;
    w_pos = buf_widx * width / buffer_len;

    DEBUGF("|");
    for (i=0; i <= width; i++)
    {
        if (i != r_pos && i != w_pos)
        {
            if (buf_ridx <= buf_widx)
            {
                if (i > r_pos && i < w_pos) {
                    DEBUGF(">");
                } else {
                    DEBUGF("-");
                }
            }
            else
            {
                if (i > r_pos || i < w_pos) {
                    DEBUGF(">");
                } else {
                    DEBUGF("-");
                }
            }
        }
        else
        {
            if (i == r_pos && i == w_pos)
            {
                if (buf_ridx <= buf_widx) {
                    DEBUGF("RW");
                } else {
                    DEBUGF("WR");
                }
            } else if (i == r_pos) {
                DEBUGF("R");
            } else if (i == w_pos) {
                DEBUGF("W");
            }
        }
    }
    DEBUGF("|");
    DEBUGF("\n");
}

void buffer_init(void)
{
    buffer = (char *)malloc(sizeof(char) * (BUFFER_SIZE + GUARD_SIZE));
    if (!buffer)
    {
        DEBUGF("couldn't allocate buffer\n");
        exit(1);
    }
    buffer_len = BUFFER_SIZE;
    guard_buffer = buffer + BUFFER_SIZE;

    buf_widx = 0;
    buf_ridx = 0;

    first_handle = NULL;
    num_handles = 0;

    conf_filechunk = AUDIO_DEFAULT_FILECHUNK;
}

/* returns true if the file still has some on disk unread */
bool handle_has_data(int handle)
{
    struct memory_handle *m = find_handle(handle);
    if (m)
    {
        return m->filerem != 0;
    }
    return false;
}

bool need_rebuffer(void)
{
    size_t free, wasted;
    wasted = first_handle? RINGBUF_SUB(first_handle->ridx, first_handle->data) : 0;
    while (wasted > buffer_len / 2)
    {
        free_buffer(first_handle->id);
        wasted = first_handle? RINGBUF_SUB(first_handle->ridx, first_handle->data) : 0;
    }
    free = buffer_len - BUF_USED;
    return ((free >= buffer_len/4));
}

bool disk_is_spinning(void)
{
    return true;
}

#define MAX_HANDLES 64
int main(int argc, char *argv[])
{
    int next_file = 1;
    int last_handle = -1;
    int handle_order[MAX_HANDLES];
    int reading_handle = 0;
    bool done_buffering = false, done_playing = false;
    //char read_buffer[GUARD_SIZE];
    unsigned char *data;
    int fd = -1; /* used to write the files out as they are read */
    int current_handle = -1;
    struct memory_handle *m = NULL;
    buffer_init();

    if (!test_ll())
    {
        DEBUGF("linked list test failed\n");
        exit(1);
    }

    buffer_init();

    while (!done_buffering || !done_playing)
    {
        DEBUGF("buffer usage: %d handles_used: %d\n", BUF_USED, num_handles);
        graph_view(100);

        /* "Buffering thread" section */
        if (!done_buffering && need_rebuffer() && disk_is_spinning())
        {
            m = find_handle(current_handle);
            if ( !m || ((m->filerem == 0) && (m->next == NULL)))
            {
                int h = bufopen(argv[next_file], 0);
                m = find_handle(h);
                if (h >= 0)
                {
                    next_file++;
                    //DEBUGF("new handle %d\n",h);
                    last_handle++;
                    handle_order[last_handle] = h;
                    buffer_handle(m->id);
                }
                current_handle = h;
            }
            else
            {
                if (m->filerem == 0)
                {
                    m = m->next;
                    current_handle = m?m->id:-1;
                }
                if (m)
                {
                    DEBUGF("buffering handle %d\n",m->id);
                    buffer_handle(m->id);
                }
            }

            if (next_file == argc && m->filerem == 0)
                done_buffering = true;
        }
        /* "Playback thread" section */
        else
        {
            DEBUGF("reading handle: %d\n", handle_order[reading_handle]);
            long read;
            long total = 0;
            char file[MAX_PATH];
            if (reading_handle >= last_handle
                && !handle_has_data(handle_order[reading_handle]))
                done_playing = true;

            if (fd < 0)
            {
                snprintf(file, MAX_PATH, "./file%d.mp3", reading_handle);
                fd = open(file, O_CREAT|O_TRUNC|O_WRONLY, 0770);
                if (fd < 0)
                {
                    DEBUGF("ERROROROROR\n");
                    exit(1);
                }
            }
            do
            {
                //read = bufread(handle_order[reading_handle], GUARD_SIZE,read_buffer);
                //write(fd, read_buffer, read);
                read = bufgetdata(handle_order[reading_handle], GUARD_SIZE, &data);
                read = MIN(read, GUARD_SIZE);
                write(fd, data, read);
                total += read;
                bufadvance(handle_order[reading_handle], read);
            }
            while (read > 0);

            DEBUGF("read %ld bytes from handle %d\n", total, handle_order[reading_handle]);

            /* close the fd/handle if there is no more data or an error */
            if (read < 0 || handle_has_data(handle_order[reading_handle]) == false)
            {
                DEBUGF("finished reading %d\n",handle_order[reading_handle]);
                bufclose(handle_order[reading_handle]);
                close(fd);
                reading_handle++;
                fd = -1;
            }
            else
            {
                DEBUGF("there is data left to buffer for %d\n", handle_order[reading_handle]);
            }
        }
    }

    DEBUGF("buffer usage: %d handles_used: %d\n", BUF_USED,num_handles);
    graph_view(100);

    free(buffer);
    return 0;
}
